import * as React from "react";
import { render } from "react-dom";
import Combobo from "combobo";

function init() {
  function wrapNum(str, val) {
    if (!val || str.toLowerCase().indexOf(val.toLowerCase()) === -1) {
      return str;
    }
    var regex = new RegExp(val, 'i');
    return str.replace(regex, '<span className="underline">$&</span>');
  }

  var box = {
    'multiselect': new Combobo({
      input: '#combobox-multiselect',
      list: '.multiselect .listbox',
      activeClass: 'active',
      multiselect: true,
      selectionValue: function (selectedOptions) {
        return selectedOptions.length > 1 ?
          '{ ' + selectedOptions.length +  ' selected }' :
          selectedOptions[0].innerText.trim();
      },
      noResultsText: 'Nothing to see here folks.',
      optionValue: function (option) {
        var inputVal = box.multiselect.input.value;
        return wrapNum(option.innerText, inputVal)
      }
    }),
  };

  window.box = box;
}

function App() {
  const [toggle, updateToggle] = React.useState("close");

  function setToggle() {
    if (toggle === "close") updateToggle("open");
    else updateToggle("close");
  }

  React.useEffect(() => {
    init();
  }, [])

  return (
    <section className="multiselect">
      <div className="wrp">
        <h2>Multi select</h2>
        <label htmlFor="combobox-multiselect">Choose Favorite Foods</label>
        <div className="combo-wrap">
          <input type="text" className="combobox" id="combobox-multiselect" />
          <i aria-hidden="true" className="fa fa-caret-down" onClick={setToggle} />
          <div className={`listbox ${toggle}`}>
            <div className="option">Pizza</div>
            <div className="option">Sushi</div>
            <div className="option">Tacos</div>
            <div className="option">Salmon</div>
            <div className="option">Cheeseburger</div>
            <div className="option">Bacon Cheeseburger</div>
            <div className="option">Steak</div>
            <div className="option">Fried Chicken</div>
            <div className="option">Chicken Salad</div>
            <div className="option">Tatar Tots</div>
            <div className="option">French Fries</div>
          </div>
        </div>
      </div>
      <button type="button">Submit</button>
    </section>
  );
}

render(<App />, document.querySelector("#root"));
